/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.animalabstract;

/**
 *
 * @author Admin
 */
public class Crab extends AquaticAnimal {
    private String nickName;
    
    public Crab(String nickName) {
        super("Crab");
        this.nickName = nickName;
    }

    @Override
    public void swim() {
        System.out.println("Crab: " + nickName + " swim");    
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + nickName + " eat");  
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + nickName + " walk");  
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + nickName + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + nickName + " sleep");  
    }
}
