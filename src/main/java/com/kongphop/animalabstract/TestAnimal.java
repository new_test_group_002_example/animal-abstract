/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.animalabstract;

/**
 *
 * @author Admin
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        System.out.println("a1 is aquatic animal ? " + (a1 instanceof AquaticAnimal));
        System.out.println("a1 is poultry animal ? " + (a1 instanceof Poultry));
        
        lineNewAnimal();
        
        Cat c1 = new Cat("Som");
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        c1.run();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));

        Animal a2 = c1;
        System.out.println("a2 is land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal ? " + (a2 instanceof Reptile));
        System.out.println("a2 is aquatic animal ? " + (a2 instanceof AquaticAnimal));
        System.out.println("a2 is poultry animal ? " + (a2 instanceof Poultry));
        
        lineNewAnimal();
        
        Dog d1 = new Dog("Mumu");
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        d1.run();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));

        Animal a3 = d1;
        System.out.println("a3 is land animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ? " + (a3 instanceof Reptile));
        System.out.println("a3 is aquatic animal ? " + (a3 instanceof AquaticAnimal));
        System.out.println("a3 is poultry animal ? " + (a3 instanceof Poultry));
        
        lineNewAnimal();
        
        Snake s1 = new Snake("Omo");
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        s1.crawl();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile animal ? " + (s1 instanceof Reptile));

        Animal a4 = s1;
        System.out.println("a4 is land animal ? " + (a4 instanceof LandAnimal));
        System.out.println("a4 is reptile animal ? " + (a4 instanceof Reptile));
        System.out.println("a4 is aquatic animal ? " + (a4 instanceof AquaticAnimal));
        System.out.println("a4 is poultry animal ? " + (a4 instanceof Poultry));
        
        lineNewAnimal();
        
        Crocodile cr1 = new Crocodile("Chalawan");
        cr1.eat();
        cr1.walk();
        cr1.speak();
        cr1.sleep();
        cr1.crawl();
        System.out.println("cr1 is animal ? " + (cr1 instanceof Animal));
        System.out.println("cr1 is reptile animal ? " + (cr1 instanceof Reptile));

        Animal a5 = cr1;
        System.out.println("a5 is land animal ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is reptile animal ? " + (a5 instanceof Reptile));
        System.out.println("a5 is aquatic animal ? " + (a5 instanceof AquaticAnimal));
        System.out.println("a5 is poultry animal ? " + (a5 instanceof Poultry));
        
        lineNewAnimal();
        
        Fish f1 = new Fish("Nemo");
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        f1.swim();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));

        Animal a6 = f1;
        System.out.println("a6 is land animal ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal ? " + (a6 instanceof Reptile));
        System.out.println("a6 is aquatic animal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is poultry animal ? " + (a6 instanceof Poultry));
        
        lineNewAnimal();
        
        Crab cb1 = new Crab("Nani");
        cb1.eat();
        cb1.walk();
        cb1.speak();
        cb1.sleep();
        cb1.swim();
        System.out.println("cb1 is animal ? " + (cb1 instanceof Animal));
        System.out.println("cb1 is aquatic animal ? " + (cb1 instanceof AquaticAnimal));

        Animal a7 = cb1;
        System.out.println("a7 is land animal ? " + (a7 instanceof LandAnimal));
        System.out.println("a7 is reptile animal ? " + (a7 instanceof Reptile));
        System.out.println("a7 is aquatic animal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is poultry animal ? " + (a7 instanceof Poultry));
        
        lineNewAnimal();
        
        Bat b1 = new Bat("Dam");
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        b1.fly();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));

        Animal a8 = b1;
        System.out.println("a8 is land animal ? " + (a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal ? " + (a8 instanceof Reptile));
        System.out.println("a8 is aquatic animal ? " + (a8 instanceof AquaticAnimal));
        System.out.println("a8 is poultry animal ? " + (a8 instanceof Poultry));
        
        lineNewAnimal();
        
        Bird bi1 = new Bird("Jib");
        bi1.eat();
        bi1.walk();
        bi1.speak();
        bi1.sleep();
        bi1.fly();
        System.out.println("bi1 is animal ? " + (bi1 instanceof Animal));
        System.out.println("bi1 is poultry animal ? " + (bi1 instanceof Poultry));

        Animal a9 = bi1;
        System.out.println("a9 is land animal ? " + (a9 instanceof LandAnimal));
        System.out.println("a9 is reptile animal ? " + (a9 instanceof Reptile));
        System.out.println("a9 is aquatic animal ? " + (a9 instanceof AquaticAnimal));
        System.out.println("a9 is poultry animal ? " + (a9 instanceof Poultry));
        
        lineNewAnimal();
        
        Bat bat = new Bat("Batty");
        Plane plane = new Plane("Engine number 1");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("Dodo");
        
        Flyable[] flyables = {bat, plane};
        for(Flyable f:flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runables = {dog, plane};
        for(Runable r:runables){
            r.run();
        }
        
    }

    public static void lineNewAnimal() {
        System.out.println("-----------------------------");
    }
}
