/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.animalabstract;

/**
 *
 * @author Admin
 */
public class Snake extends Reptile {
    private String nickName;
    
    public Snake(String nickName) {
        super("Snake", 0);
        this.nickName = nickName;
    }

    @Override
    public void crawl() {
        System.out.println("Snake: " + nickName + " crawl");    
    }

    @Override
    public void eat() {
        System.out.println("Snake: " + nickName + " eat");  
    }

    @Override
    public void walk() {
        System.out.println("Snake: " + nickName + " walk");  
    }

    @Override
    public void speak() {
        System.out.println("Snake: " + nickName + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Snake: " + nickName + " sleep");  
    }
}
