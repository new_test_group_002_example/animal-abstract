/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.animalabstract;

/**
 *
 * @author Admin
 */
public class Bat extends Poultry {
    private String nickName;
    
    public Bat(String nickName) {
        super("Bat", 2);
        this.nickName = nickName;
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + nickName + " fly");    
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + nickName + " eat");  
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + nickName + " walk");  
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + nickName + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + nickName + " sleep");  
    }
}
