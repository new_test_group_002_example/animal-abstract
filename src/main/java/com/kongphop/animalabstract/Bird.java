/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.animalabstract;

/**
 *
 * @author Admin
 */
public class Bird extends Poultry {
    private String nickName;
    
    public Bird(String nickName) {
        super("Bird", 2);
        this.nickName = nickName;
    }

    @Override
    public void fly() {
        System.out.println("Bird: " + nickName + " fly");    
    }

    @Override
    public void eat() {
        System.out.println("Bird: " + nickName + " eat");  
    }

    @Override
    public void walk() {
        System.out.println("Bird: " + nickName + " walk");  
    }

    @Override
    public void speak() {
        System.out.println("Bird: " + nickName + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Bird: " + nickName + " sleep");  
    }
}
