/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.animalabstract;

/**
 *
 * @author Admin
 */
public class Fish extends AquaticAnimal {
    private String nickName;
    
    public Fish(String nickName) {
        super("Fish");
        this.nickName = nickName;
    }

    @Override
    public void swim() {
        System.out.println("Fish: " + nickName + " swim");    
    }

    @Override
    public void eat() {
        System.out.println("Fish: " + nickName + " eat");  
    }

    @Override
    public void walk() {
        System.out.println("Fish: " + nickName + " walk");  
    }

    @Override
    public void speak() {
        System.out.println("Fish: " + nickName + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Fish: " + nickName + " sleep");  
    }
}
